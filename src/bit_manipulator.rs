use std::fs::OpenOptions;
use std::io::{Read, Seek, SeekFrom, Write};
use std::path::Path;
use std::{fs::File, path::PathBuf};

use filetime::{set_file_atime, set_file_mtime, FileTime};

pub struct BitManipulator {
    path: PathBuf,
    file: File,
    atime: Option<FileTime>,
    mtime: Option<FileTime>,
}

impl BitManipulator {
    pub fn new(file_path: &Path) -> BitManipulator {
        Self::with_options(file_path, false, false)
    }

    pub fn with_options(
        file_path: &Path,
        keep_atime: bool,
        keep_mtime: bool,
    ) -> BitManipulator {
        let mut atime = None;
        let mut mtime = None;
        if keep_atime || keep_mtime {
            let meta = file_path.metadata().unwrap();
            if keep_atime {
                atime = Some(FileTime::from_last_access_time(&meta));
            }
            if keep_atime {
                mtime = Some(FileTime::from_last_modification_time(&meta));
            }
        }

        let file = Self::open_file(file_path);

        BitManipulator {
            path: file_path.to_path_buf(),
            file,
            atime,
            mtime,
        }
    }

    fn open_file(file_path: &Path) -> File {
        let file = OpenOptions::new()
            .read(true)
            .write(true)
            .open(file_path.to_str().unwrap())
            .unwrap();
        file
    }

    pub fn get_bit(&self, byte_offset: u64, bit_pos: u8) -> u8 {
        let mut file = &self.file;
        file.seek(SeekFrom::Start(byte_offset)).unwrap();
        let mut buf = [0_u8; 1];
        file.read(&mut buf).unwrap();
        let byte = buf[0];
        let mask = 1 << bit_pos;
        let bit = (byte & mask) >> bit_pos;
        return bit;
    }

    pub fn set_bit(&self, byte_offset: u64, bit_pos: u8, bit: u8) {
        let mut file = &self.file;
        file.seek(SeekFrom::Start(byte_offset)).unwrap();
        let mut buf = [0_u8; 1];
        file.read(&mut buf).unwrap();
        let mask = 1 << bit_pos;

        let byte = buf[0];
        let byte = if bit == 0 { byte & !mask } else { byte | mask };

        buf[0] = byte;

        // TODO: Error if byte offset is larger than file.stream_len
        file.seek(SeekFrom::Start(byte_offset)).unwrap();
        file.write(&buf).unwrap();
    }
}

impl Drop for BitManipulator {
    fn drop(&mut self) {
        if let Some(atime) = self.atime {
            set_file_atime(&self.path, atime).unwrap();
        }
        if let Some(mtime) = self.mtime {
            set_file_mtime(&self.path, mtime).unwrap();
        }
    }
}
