use bitman::{run, Opts};
use clap::Clap;
use std::process;

fn main() {
    let opts = Opts::parse();

    if let Err(e) = run(opts) {
        eprintln!("ERROR: {}", e);
        process::exit(1);
    }
}
