use crate::action::Action;
use crate::operation::Operation;
use chrono::{DateTime, Local};
use serde::{Deserialize, Serialize};
use serde_json as json;
use serde_json::ser::{Formatter, PrettyFormatter};
use std::error::Error;
use std::fs::File;
use std::fs::OpenOptions;
use std::io::{self, Write};
use std::path::Path;

pub struct LogFile {
    file: File,
    contents: Vec<LogObj>,
}

#[derive(Serialize, Deserialize)]
#[serde(untagged)]
pub enum LogObj {
    Action {
        #[serde(with = "my_date_format")]
        date: DateTime<Local>,
        action: Action,
    },
    Operation {
        #[serde(with = "my_date_format")]
        date: DateTime<Local>,
        operation: Operation,
    },
}

impl LogFile {
    pub fn new(path: &Path) -> Result<LogFile, Box<dyn Error>> {
        let file = Self::open_file(&path)?;
        // TODO: This is not necessary, to make sure the log is readable. This
        //       should be put in a log reader and should then be called from the main
        //       program so the main program can verify that the log is readable.
        let contents = Self::parse_log(&file)?;
        let log = Self { file, contents };

        Ok(log)
    }

    fn open_file(path: &Path) -> Result<File, String> {
        OpenOptions::new()
            .read(true)
            .write(true)
            .create(true)
            .append(true)
            .open(path)
            .or_else(|e| Err(format!("Failed to open logfile: {}", e)))
    }

    fn parse_log(file: &File) -> Result<Vec<LogObj>, String> {
        let stream = serde_json::Deserializer::from_reader(file);

        let mut objects = vec![];

        for x in stream.into_iter() {
            let log_obj: LogObj = x.or_else(|e| {
                use json::error::Category as C;
                let cause = match e.classify() {
                    C::Syntax => "Syntax error",
                    C::Data => "Wrong type",
                    C::Io => "IO error",
                    C::Eof => "Reached EOF",
                };
                Err(format!("Failed to parse logfile: {}: {}", cause, e))
            })?;
            objects.push(log_obj);
        }
        Ok(objects)
    }

    fn write_obj(&self, obj: &LogObj) -> Result<(), io::Error> {
        let file = &self.file;

        let format = LogFormatter::default();
        let mut ser = serde_json::Serializer::with_formatter(file, format);

        obj.serialize(&mut ser)?;

        file.sync_all()?;

        Ok(())
    }

    pub fn log_action(&mut self, action: Action) {
        let obj = LogObj::Action {
            date: chrono::Local::now(),
            action,
        };
        self.write_obj(&obj).unwrap();
        self.contents.push(obj);
    }

    pub fn log_operation(&mut self, operation: Operation) {
        let obj = LogObj::Operation {
            date: chrono::Local::now(),
            operation,
        };
        self.write_obj(&obj).unwrap();
        self.contents.push(obj);
    }

    pub fn contents(&self) -> &Vec<LogObj> {
        &self.contents
    }
}

mod my_date_format {
    use chrono::{DateTime, Local, TimeZone};
    use serde::{self, Deserialize, Deserializer, Serializer};

    const FORMAT_RFC3339: &'static str = "%Y-%m-%dT%H:%M:%S%.9f%:z";

    // The signature of a serialize_with function must follow the pattern:
    //
    //    fn serialize<S>(&T, S) -> Result<S::Ok, S::Error>
    //    where
    //        S: Serializer
    //
    // although it may also be generic over the input types T.
    pub fn serialize<S>(
        date: &DateTime<Local>,
        serializer: S,
    ) -> Result<S::Ok, S::Error>
    where
        S: Serializer,
    {
        let s = format!("{}", date.format(FORMAT_RFC3339));
        serializer.serialize_str(&s)
    }

    // The signature of a deserialize_with function must follow the pattern:
    //
    //    fn deserialize<'de, D>(D) -> Result<T, D::Error>
    //    where
    //        D: Deserializer<'de>
    //
    // although it may also be generic over the output types T.
    pub fn deserialize<'de, D>(
        deserializer: D,
    ) -> Result<DateTime<Local>, D::Error>
    where
        D: Deserializer<'de>,
    {
        let s = String::deserialize(deserializer)?;
        Local
            .datetime_from_str(&s, FORMAT_RFC3339)
            .map_err(serde::de::Error::custom)
    }
}

#[derive(Default)]
struct LogFormatter {
    pretty: PrettyFormatter<'static>,
    depth: usize,
}

impl Formatter for LogFormatter {
    fn begin_object<W: ?Sized + Write>(&mut self, w: &mut W) -> io::Result<()> {
        self.depth += 1;
        self.pretty.begin_object(w)
    }
    fn end_object<W: ?Sized + Write>(&mut self, w: &mut W) -> io::Result<()> {
        self.pretty.end_object(w).and_then(|()| {
            self.depth -= 1;
            if self.depth == 0 {
                w.write_all(b"\n")
            } else {
                Ok(())
            }
        })
    }
}
