mod action;
mod bit_manipulator;
pub mod cli;
mod log_file;
mod operation;

use action::Action;
use bit_manipulator::BitManipulator;
use cli::Restore;
pub use cli::{
    File, FileSubCommand, Get, GetBit, GetSubCommand, Negate, NegateBit,
    NegateSubCommand, Opts, Set, SetBit, SetSubCommand, SubCommand,
};
use log_file::{LogFile, LogObj};
use operation as op;
use operation::Operation;
use std::error::Error;
use std::path::{Path, PathBuf};

pub fn run(opts: Opts) -> Result<(), Box<dyn Error>> {
    let log_path = Path::new(&opts.log);
    let log = LogFile::new(log_path)?;

    match &opts.subcmd {
        SubCommand::Restore(sc) => restore(&opts, sc, log)?,
        SubCommand::File(sc) => file(&opts, sc, log)?,
    };

    Ok(())
}

fn restore(
    opts: &Opts,
    restore_opts: &Restore,
    mut log: LogFile,
) -> Result<(), Box<dyn Error>> {
    let force_all = restore_opts.force_all;

    let restore_from = Path::new(&restore_opts.log_file);
    let restore_log = LogFile::new(restore_from)?;

    let mut operations = vec![];
    let mut next_operations = vec![];
    for obj in restore_log.contents().iter().rev() {
        match obj {
            // Scan for next action
            LogObj::Action { action, .. } => match action {
                // If not `force_all` stop, when encountering a restore action
                Action::Restore { .. } => {
                    if !force_all {
                        break;
                    }
                }
                // Add collected operations to the `operations` vector
                _ => operations.append(&mut next_operations),
            },
            // Collect all operations
            LogObj::Operation { operation, .. } => {
                next_operations.push(operation.revert())
            }
        }
    }

    log.log_action(Action::Restore {
        log_file: restore_opts.log_file.clone(),
        force_all: restore_opts.force_all,
    });

    eprintln!("Restoring {} operations", operations.len());

    for op in operations {
        match op {
            Operation::SetBit(op) => {
                set_bit_raw(op, &mut log, opts.keep_atime, opts.keep_mtime)
            }
        }
    }

    Ok(())
}

fn file(
    opts: &Opts,
    file_opts: &File,
    log: LogFile,
) -> Result<(), Box<dyn Error>> {
    let file = &file_opts.file;
    let file = std::fs::canonicalize(file).unwrap();

    match &file_opts.subcmd {
        FileSubCommand::Get(sc) => match &sc.subcmd {
            GetSubCommand::Bit(sc) => get_bit(sc, file),
        },
        FileSubCommand::Set(sc) => match &sc.subcmd {
            SetSubCommand::Bit(sc) => set_bit(opts, sc, file, log),
        },
        FileSubCommand::Negate(sc) => match &sc.subcmd {
            NegateSubCommand::Bit(sc) => negate_bit(sc, file, log)?,
        },
    };

    Ok(())
}

fn get_bit(opts: &GetBit, file: PathBuf) {
    let bm = BitManipulator::new(&file);
    let bit = bm.get_bit(opts.offset, opts.bit);

    println!("{}", bit)
}

fn set_bit(
    opts: &Opts,
    set_bit_opts: &SetBit,
    file: PathBuf,
    mut log: LogFile,
) {
    log.log_action(Action::FileSetBit {
        file: file.clone(),
        offset: set_bit_opts.offset,
        bit: set_bit_opts.bit,
        new_value: set_bit_opts.value,
    });

    let operation = op::SetBit {
        file,
        offset: set_bit_opts.offset,
        bit: set_bit_opts.bit,
        orig_value: None,
        new_value: set_bit_opts.value,
    };

    set_bit_raw(operation, &mut log, opts.keep_atime, opts.keep_mtime);
}

fn set_bit_raw(
    mut op: op::SetBit,
    log: &mut LogFile,
    keep_atime: bool,
    keep_mtime: bool,
) {
    let bm = BitManipulator::with_options(&op.file, keep_atime, keep_mtime);
    let orig_bit = bm.get_bit(op.offset, op.bit);
    op.orig_value = Some(orig_bit);

    log.log_operation(Operation::SetBit(op::SetBit {
        orig_value: Some(orig_bit),
        ..op
    }));
    bm.set_bit(op.offset, op.bit, op.new_value);
}

fn negate_bit(
    opts: &NegateBit,
    file: PathBuf,
    mut log: LogFile,
) -> Result<(), Box<dyn Error>> {
    log.log_action(Action::FileNegateBit {
        file: file.clone(),
        offset: opts.offset,
        bit: opts.bit,
    });

    let bm = BitManipulator::new(&file);

    let orig_bit = bm.get_bit(opts.offset, opts.bit);

    let new_value = if orig_bit == 0 { 1 } else { 0 };

    log.log_operation(Operation::SetBit(op::SetBit {
        file: file.clone(),
        offset: opts.offset,
        bit: opts.bit,
        orig_value: Some(orig_bit),
        new_value,
    }));

    bm.set_bit(opts.offset, opts.bit, new_value);

    Ok(())
}
