use clap::{Clap, crate_authors, crate_version};
pub use super::file::*;
pub use super::restore::*;

/// A tool to flip bits in files
#[derive(Clap)]
#[clap(version = crate_version!(), author = crate_authors!(), )]
pub struct Opts {
    /// Logfile to track and revert changes
    #[clap(short, long, default_value = "./bitman.log")]
    pub log: String,
    /// Do not change "access time" of modified files
    #[clap(short = 'a', long)]
    pub keep_atime: bool,
    /// Do not change "modification time" of modified files
    #[clap(short = 'm', long)]
    pub keep_mtime: bool,

    #[clap(subcommand)]
    pub subcmd: SubCommand,
}

#[derive(Clap)]
pub enum SubCommand {
    Restore(Restore),
    File(File),
}

