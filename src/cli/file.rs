mod get;
mod negate;
mod set;

use clap::Clap;
pub use get::*;
pub use negate::*;
pub use set::*;
use std::path::PathBuf;

/// Work with a file
#[derive(Clap)]
pub struct File {
    /// The file to work with
    pub file: PathBuf,
    #[clap(subcommand)]
    pub subcmd: FileSubCommand,
}

#[derive(Clap)]
pub enum FileSubCommand {
    Get(Get),
    Set(Set),
    #[clap(alias = "neg", alias = "flip")]
    Negate(Negate),
}
