use clap::Clap;
use std::path::PathBuf;

/// Restore changes from a logfile
#[derive(Clap)]
pub struct Restore {
    /// The logfile to restore from
    pub log_file: PathBuf,
    /// Restore all changes mentioned in the logfile, not only those up to the
    /// last restore action
    #[clap(long)]
    pub force_all: bool,
}
