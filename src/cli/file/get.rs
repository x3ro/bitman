use clap::Clap;

/// Read bits
#[derive(Clap)]
pub struct Get {
    #[clap(subcommand)]
    pub subcmd: GetSubCommand,
}

#[derive(Clap)]
pub enum GetSubCommand {
    Bit(GetBit),
}

/// Print a specific bit
#[derive(Clap)]
pub struct GetBit {
    /// Offset of the byte
    pub offset: u64,
    /// Bit position of the selected byte (LSB=0, MSB=7)
    #[clap(possible_values = &["0","1","2","3","4","5","6","7"])]
    pub bit: u8,
}
