use clap::Clap;

/// Negate bits
#[derive(Clap)]
pub struct Negate {
    #[clap(subcommand)]
    pub subcmd: NegateSubCommand,
}

#[derive(Clap)]
pub enum NegateSubCommand {
    Bit(NegateBit),
}

/// Negate (flip) a specific bit
#[derive(Clap)]
pub struct NegateBit {
    /// Offset of the byte
    pub offset: u64,
    /// Bit position of the selected byte (LSB=0, MSB=7)
    #[clap(possible_values = &["0","1","2","3","4","5","6","7"])]
    pub bit: u8,
}
