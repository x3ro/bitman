use clap::Clap;

/// Change bits
#[derive(Clap)]
pub struct Set {
    #[clap(subcommand)]
    pub subcmd: SetSubCommand,
}

#[derive(Clap)]
pub enum SetSubCommand {
    Bit(SetBit),
}

/// Change a specific bit
#[derive(Clap)]
pub struct SetBit {
    /// Offset of the byte
    pub offset: u64,
    /// Bit position of the selected byte (LSB=0, MSB=7)
    #[clap(possible_values = &["0","1","2","3","4","5","6","7"])]
    pub bit: u8,
    /// Value the bit should be set to
    #[clap(possible_values = &["0","1"])]
    pub value: u8,
}
