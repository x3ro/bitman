use serde::{Deserialize, Serialize};
use std::path::PathBuf;

#[derive(Serialize, Deserialize)]
pub enum Action {
    Restore{
        log_file: PathBuf,
        force_all: bool,
    },
    FileSetBit {
        file: PathBuf,
        offset: u64,
        bit: u8,
        new_value: u8,
    },
    FileNegateBit {
        file: PathBuf,
        offset: u64,
        bit: u8,
    },
}
