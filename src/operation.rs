use serde::{Deserialize, Serialize};
use std::path::PathBuf;

#[derive(Serialize, Deserialize, Clone)]
pub enum Operation {
    SetBit(SetBit),
}

#[derive(Serialize, Deserialize, Clone)]
pub struct SetBit {
    pub file: PathBuf,
    pub offset: u64,
    pub bit: u8,
    // TODO: Make sure this is not serializable when value is `None`
    pub orig_value: Option<u8>,
    pub new_value: u8,
}

impl Operation {
    pub fn revert(&self) -> Operation {
        match self.clone() {
            Operation::SetBit(op) => Operation::SetBit(SetBit {
                orig_value: None,
                new_value: op.orig_value.unwrap(),
                ..op
            }),
        }
    }
}
